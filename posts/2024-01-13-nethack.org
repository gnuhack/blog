#+title: Nethack
#+date: <2024-01-13 19:14>
#+description: 
#+filetags: nethack, juegos, software libre
La primera vez que jugué al [[https://www.thegreatestgameyouwilleverplay.com/][Nethack]] lo hice simplemente por
curiosidad. Quería ver si era posible hacer un juego cuyos gráficos
fueran tan solo caracteres ASCII. También estaba interesado en ver
cómo era el primer /roguelike/, ya que Nethack deriva del Hack que a
su vez es una evolución de Rogue, que fue el primer juego del género
al que da nombre.

No sé si fue porque ya estaba acostumbrado por el Dwarf Fortress a ver
gráficos en ASCII, pero no me costó empezar a jugar, y para mi
sorpresa, descubrí que era un juego increíblemente entretenido, que
era algo más que una simple curiosidad.

El objetivo del juego es de lo más simple. Tienes que bajar a las
mazmorras para conseguir el amuleto de Yendor y entregárselo a tu
Dios.

Pero realmente en una partida de Nethack puede suceder de todo, y para
muestra dejo un resumen de algunas de mis muertes favoritas en el juego.

** La de los hombres-rata
Mi camino quedó bloqueado por una piedra enorme. Me estaban acosando
unos hombres rata, así que me dejé morder por uno de ellos, me
transformé en rata, pasé por el hueco entre la piedra y la pared y
luego usé la habilidad que tienen los hombres rata de llamar a una
jauría de ratas. Me enfrenté con mi equipo de ratas contra el del otro
hombre rata, pero desafortunadamente perdí y morí.

** La de la ninfa +de los cojones+
Conseguí un amuleto al inicio de la partida, lo identifiqué con un
pergamino de identificación y vi que era un amuleto de reflejo, un
objeto extremadamente valioso y raro. Al instante me lo puse, deseoso
de estrenarlo contra algún hechizo enemigo.

Mi perro se cargó a un monstruo que al morir explotó dejándome ciego,
y mientras estaba ciego una ninfa me quitó el amuleto y una varita que
no había identificado, así que no sabía qué hacía.

Cuando recuperé la vista, fui en busca de la ninfa a recuperar mi
preciado amuleto, para acabar muerto a manos de mi varita, que la ninfa
(con mi amuleto puesto) había agitado, y había resultado ser una varita
de fuego que me chamuscó, y morí.

** La cocatriz
Si una cocatriz te ataca puedes convertirte en piedra, pero si las
coges con guantes y las usas como arma puedes convertir en piedra a los
enemigos. Yo iba muy contento aporreando enemigos con mi cocatriz. De
hecho estaba tan contento que se me olvidó comer, me desmayé por el
hambre, caí sobre la cocatriz y me convertí en piedra.

** Y muchas otras más...
He mencionado algunas de mis partidas, pero en los años que he jugado
he tenido una gran variedad de muertes, por cosas tan variadas como un
abrelatas maldito o equivocarme de tecla y beber la poción que no era.

Así que, por ser un juego tremendamente entretenido, por condensarlo
todo en poco más de 1 megabyte, por ser software libre, y por muchas
otras cosas, creo que Nethack es el mejor juego que existe.
