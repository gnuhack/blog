#+title: 2FA en Eshell
#+date: <2024-03-11 15:03>
#+description: 
#+filetags: emacs, eshell, 2FA
Ahora que la universidad pide el código de verificación en dos pasos
hasta para respirar, decidí hacer una función en Eshell que me
facilitara la obtención de dicho código.

Tenía una función básica, que no recuerdo dónde la saqué, pero
requería tener un búfer de Eshell en segundo plano para que
funcionara.

#+begin_src emacs-lisp
  (defun eshell2fa ()
      (interactive)
      "Devuelve el id del 2FA en el portapapeles."
      (with-current-buffer "*eshell*"
      (eshell-return-to-prompt)
      (insert "id > /dev/kill")
      (eshell-send-input))
      )
#+end_src
La línea del =insert= es la que se encarga de obtener la clave a
través del alias =id=, que usa =oathtool= para obtener el
código. Luego se redirige al portapapeles con => /dev/kill=.

El alias se define de la siguiente forma:
=alias id oathtool --counter=30 --totp=SHA1 -d 6 -b "clave-de-2FA"=

Para mejorarla he añadido una línea que crea el búfer de Eshell si no
ha sido creado anteriormente, con un =unless=
#+begin_src emacs-lisp
  (defun eshell2fa ()
      (interactive)
      "Devuelve el id del 2FA en el portapapeles."
      (unless (gnus-buffer-live-p "*eshell*") (eshell) (switch-to-prev-buffer))
      (with-current-buffer "*eshell*"
      (eshell-return-to-prompt)
      (insert "id > /dev/kill")
      (eshell-send-input))
      )
#+end_src

Y luego sólo queda asignarla (por ejemplo a =f12=) para poder tener el
código pulsando un botón.

#+begin_src emacs-lisp
  (global-set-key (kbd "<f12>") 'eshell2fa)
#+end_src
