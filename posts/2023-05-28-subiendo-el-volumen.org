#+title: Subiendo el volumen
#+date: <2023-05-28 12:45>
#+description: 
#+filetags: alsa,audio,i3wm
Por alguna razón, en mi Thinkpad X230 a veces el volumen al 100% sigue siendo demasiado bajo. Pero como es posible subir el volumen bastante por encima del 100%, he implementado un atajo de teclado en el archivo de configuración de i3wm que me permita subir el volumen todo lo que quiera.

Simplemente hay que añadir las siguientes líneas al config de i3:

=bindsym Shift+XF86AudioRaiseVolume exec pactl -- set-sink-volume 0 +10% && pkill -RTMIN+10 i3blocks=
=bindsym Shift+XF86AudioLowerVolume exec pactl -- set-sink-volume 0 -10% && pkill -RTMIN+10 i3blocks=

Y así, al pulsar shift y el botón de subir o bajar el volumen, se subirá por encima del 100% o se bajará en incrementos del 10%.
