#+title: Guía para leer a Gene Wolfe
#+date: <2023-04-01 11:05>
#+description: 
#+filetags: literatura, Gene, Wolfe

Gracias a la tecnología RSS descubrí hace un par de meses el mundo de Gene Wolfe a través del blog "Take on Rules"
(https://takeonrules.com/2022/05/30/on-gathering-an-online-group-for-gaming/), en el que se cita un fragmento del libro
"La Ciudadela del Autarca" (aunque el resto del post no tiene nada que ver con el libro). Me pareció curioso el título
del libro y tenía un presentimiento de que me iba a gustar. 

Tanto ha sido así que estoy ya me lo he leído cuatro veces, y desde que descubrí las infinitas capas de profundidad que da
Gene Wolfe a sus relatos se ha convertido probablemente en el libro que más he disfrutado de todos los que he leído. 

Es un escritor que ya en el mundo anglosajón es poco conocido (es conocido entre escritores, pero no gusta tanto a los
lectores), cuanto más en España, por lo que he decidido dejar aquí adaptada en español la guía sobre cómo leer a Gene
Wolfe que hizo en su día Neil Gaiman, con el objetivo de facilitar su lectura. Son libros densos, cuya lectura requiere
poner atención, estar concentrado y atento a los detalles, pero es altamente gratificante ir descifrando los misterios y
el universo que propone Wolfe. 

Según Gaiman, para disfrutar la lectura de Gene Wolfe hay que: 

1. Confiar en el texto implícitamente. Las respuestas están ahí.
2. No te fíes del texto a ciegas. Está lleno de trucos y engaños, el narrador no es una persona en la que se pueda
 confiar.
3. Vuelve a leer. Es mejor la segunda vez. Será aún mejor la tercera vez. Y de todos modos, los libros se transformarán
 sutilmente mientras estés lejos de ellos. El libro "Paz" realmente era una memoria agradable del Medio Oeste la primera
 vez que lo leí. Solo se convirtió en una novela de terror en la segunda o tercera lectura.
4. Hay lobos ahí dentro, acechando detrás de las palabras. A veces salen en las páginas. A veces esperan hasta que cierres
 el libro. El almizcleño olor a lobo a veces puede ser enmascarado por el perfume aromático del romero. Ten en cuenta
 que estos no son lobos de hoy, que se arrastran grises en manadas por lugares desiertos. Estos son lobos excepcionales,
 enormes y solitarios que podrían mantenerse en pie frente a los osos pardos.
5. Leer a Gene Wolfe es un trabajo peligroso. Es un acto de lanzar cuchillos, y como todos los buenos actos de lanzar
 cuchillos, puedes perder dedos, dedos de los pies, lóbulos de las orejas o ojos en el proceso. A Gene se la
 refanfinfla, él es el que está lanzando los cuchillos.
6. Ponte cómodo. Sirve una tetera de té. Cuelga una señal de NO MOLESTAR. Comienza en la página uno.
7. Hay dos tipos de escritores astutos. Los que señalan cuán astutos son, y los que no ven la necesidad de señalar cuán
 astutos son. Gene Wolfe es del segundo tipo, y la inteligencia es menos importante que la historia. No es inteligente
 para hacerte sentir estúpido. Es inteligente para hacerte inteligente también.
8. Él estaba allí. Lo vio suceder. Sabe cuál fue la reflexión que vieron en el espejo esa noche.
9. Estar dispuesto a aprender.
