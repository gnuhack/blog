#+title: Calendario para la org-agenda
#+date: <2025-02-02 19:20>
#+description: 
#+filetags: emacs, org-mode, org-agenda

La agenda del org-mode de Emacs muestra por defecto una lista diaria,
semanal o mensual de las citas o tareas que tengamos en nuestro
archivo de agenda. Sin embargo no tiene una vista tipo calendario por
defecto. Para ello se puede usar el paquete =calfw-org=, disponible en
MELPA.

Después de instalarlo sólo hay que cargarlo con =(require 'calfw-org)=
y ejecutar la orden =M-x cfw:open-org-calendar= para ver nuestra
agenda en formato calendario.

Además, aprovechando que el x270 que tengo tiene pantalla táctil he
decidido probar a asignarle esta orden a un toque de la pantalla para
que aparezca la agenda en modo calendario.

Para la asignación en vez de código he usado Embark. Una vez abierto
el menú de Emacs con =M-x=, y estando sobre la orden
=cfw:open-org-calendar=, invoco Embark con =C-.= y luego pulso =g=
para asignar un atajo de teclado global. Después pulso la pantalla y
ya tengo configurado todo para poder ver mi agenda al toque.

