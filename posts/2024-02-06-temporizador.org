#+title: Temporizador
#+date: <2024-02-06 20:08>
#+description: 
#+filetags: emacs, rubik, GNU/Linux

Ayer estaba en el césped de la Escuela en un tiempo muerto entre
clases y se me ocurrió practicar un poco con el cubo 3x3 que tengo (un
Yuxin Little Magic magnético, si no recuerdo mal), cronometrando mis tiempos.

Pero desde hace un tiempo no consigo que me funcione la red de la
universidad, y, como hace relativamente poco que me instalé Debian 12
en mi Thinkpad X230, no tenía ningún programa para cronometrar el
tiempo que tardo en hacer el cubo.

Miré el manual de Elisp, pero los temporizadores que hay sólo llegan a
precisión de segundos, y yo necesitaba hasta las centésimas de
segundo. Recordé que en informática de cuarto dimos temporizadores
POSIX, pero no tenía ganas de ponerme a hacer un programa en C y tener
que mirarme el estándar sólo para hacer un pequeño programa.

Me acordé de que existe un programa en GNU/Linux que se llama =time=,
que cronometra el tiempo que tarda en ejecutarse un comando. Estuve
mirando entre los programas en C que tenía de informática hechos por
el profesor, y había uno que se llamaba =pierde_tiempo.c=.

El programa es tan sólo un bucle infinito, y sirve para demostrar el
funcionamiento de los hilos y los procesos en C + POSIX. Pero lo
interesante es que al combinarlo con time puedo saber con precisión
de milésimas de segundo cuánto tiempo ha estado ejecutándose el
programa hasta que lo pare con =C-c=.

Así, con una sola línea en la terminal, improvisé un cronómetro casero
para poder cronometrarme con una precisión de centésimas de
segundo. Añadí una macro de Emacs para poder pausar el programa con
F12 y no perder tiempo pulsando =C-c=, y ya tenía todo listo, y sin
usar internet en ningún momento.
