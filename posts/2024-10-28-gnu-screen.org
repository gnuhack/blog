#+title: GNU Screen
#+date: <2024-10-28 23:23>
#+description: 
#+filetags: GNU/Linux

Normalmente uso Emacs para gestionar las diferentes sesiones de
terminal que tengo abiertas, pero a veces también uso un emulador de
terminal fuera de Emacs.

Como echaba en falta poder gestionar distintas terminales sin tener
que estar abriendo y cerrando un montón de ventanas, estuve
aprendiendo lo básico de GNU Screen, un multiplexor de terminales.

En Debian se puede instalar desde los repositorios fácilmente con un
=sudo apt install screen=. Una vez instalado, se puede iniciar con la
orden =screen=, y nos saldrá un mensaje de bienvenida que se puede
saltar pulsando intro.

La forma básica de interactuar con Screen es pulsando =Control + a=
y después la orden correspondiente. Para ver todos los atajos de
teclado se puede pulsar =Control + a + ?=.

La interfaz de Screen es muy minimalista por defecto, tanto que de
hecho es difícil saber si estás o no dentro de Screen. Para salir del
programa basta con escribir =exit= y nos saldrá un mensaje
informándonos que la sesión de Screen ha terminado.

Dentro de Screen podemos crear terminales con =Control + a + c=,
cerrarlos con =Control + a + k= o cambiar entre terminales con
=Control + a + n= o =Control + a + p=.

Además de este uso básico, también se pueden crear distintas ventanas
dentro de una misma terminal, o crear un archivo de configuración que
ejecute scripts o configure las teclas de configuración de Screen.

Por ejemplo, para cambiar =Control + a= por =Control + t= como tecla
principal de Screen podemos incluir la siguiente línea en un archivo
llamado =.screenrc= en nuestro directorio personal:

=escape ^Tt=

En =/etc/screenrc= si estamos en Debian podemos encontrar un archivo
de configuración base para Screen para no empezar la configuración de
cero.

Con este uso básico de un multiplexor es mucho más fácil trabajar con distintas
terminales de forma cómoda y rápida.
